from src.exceptions import InvalidBitCount, InvalidOperator, NotBinary
from src.helpers.validations import validate_operator

from src.domain.calculator import Calculator


def handle_operation(obj, operator):
    validate_operator(operator)

    if operator == '+':
        return obj.sum()
    elif operator == '-':
        return obj.subtract()
    elif operator == '*':
        return obj.multiply()
    elif operator == '/':
        return obj.split()
    elif operator == '%':
        return obj.modulo()


def main():
    print('---------------------------------------------------------------------------')
    print('Cálculo aritmético de números binários.\n')
    print('NOTA¹: Os operadores aceitos são: +, -, *, / e %           <---------------')
    print('NOTA²: Serão aceitos números com apenas 8 bits             <---------------')
    print('NOTA³: Ao finalizar cada inserção pressione a tecla enter  <---------------')
    print('---------------------------------------------------------------------------\n')
    print('Digite, respectivamente, o operador, o primeiro e o segundo número binário:')
    operator = input()
    number_one = input()
    number_two = input()

    try:
        obj = Calculator(number_one, number_two)
        result = handle_operation(obj, operator)
        print('O resultado é: {}'.format(result))
    except InvalidOperator:
        print('OCORREU UM ERRO: O operador não é válido!')
    except InvalidBitCount:
        print('OCORREU UM ERRO: A quantidade de bits não é válida!')
    except NotBinary:
        print('OCORREU UM ERRO: O número informado não é binário!')


if __name__ == '__main__':
    main()
