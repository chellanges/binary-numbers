import unittest

from src.domain.calculator import Calculator


class CalculatorTestCase(unittest.TestCase):
    def test_should_sum_when_is_plus_method(self):
        num1 = '00000001'
        num2 = '00000011'

        calculator = Calculator(num1, num2)
        sum_result = calculator.sum()
        expected = '00000100'
        self.assertEqual(sum_result, expected)

    def test_should_subtract_when_is_minus_method(self):
        num1 = '00000010'
        num2 = '00000001'

        calculator = Calculator(num1, num2)
        subtract_result = calculator.subtract()
        expected = '00000001'
        self.assertEqual(subtract_result, expected)

    def test_should_multiply_when_is_multiplication_method(self):
        num1 = '00000001'
        num2 = '00000011'

        calculator = Calculator(num1, num2)
        multiply_result = calculator.multiply()
        expected = '00000011'
        self.assertEqual(multiply_result, expected)

    def test_should_split_when_is_division_method(self):
        num1 = '00010100'
        num2 = '00000011'

        calculator = Calculator(num1, num2)
        split_result = calculator.split()
        expected = '00000110'
        self.assertEqual(split_result, expected)

    def test_should_calculate_the_division_modulus_when_is_percentage_method(self):
        num1 = '00010100'
        num2 = '00000011'

        calculator = Calculator(num1, num2)
        modulo_result = calculator.modulo()
        expected = '00000010'
        self.assertEqual(modulo_result, expected)


if __name__ == '__main__':
    unittest.main()
