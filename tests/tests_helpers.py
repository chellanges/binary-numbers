import unittest

from src.exceptions import InvalidBitCount, InvalidOperator, NotBinary, NotInteger, NotString

from src.helpers.conversions import bin_to_int, int_to_bin
from src.helpers.validations import is_binary, is_integer, validate_binary, validate_operator


class ValidationsTestCase(unittest.TestCase):
    def test_should_check_if_the_number_is_binary_when_passed(self):
        self.assertTrue(is_binary('00000000'))
        self.assertTrue(is_binary('11111111'))
        self.assertTrue(is_binary('00111111'))
        self.assertTrue(is_binary('11000000'))
        self.assertTrue(is_binary('01010101'))
        self.assertTrue(is_binary('10101010'))
        self.assertFalse(is_binary('isbinary'))
        self.assertFalse(is_binary('oooooooo'))
        self.assertFalse(is_binary('1001OO01'))
        self.assertFalse(is_binary('+0100110'))

    def test_should_check_if_the_number_is_integer_when_passed(self):
        self.assertTrue(is_integer(255))
        self.assertFalse(is_integer('11111111'))

    def test_should_throw_exception_when_a_string_is_not_passed(self):
        with self.assertRaises(NotString):
            validate_binary(10011011)

    def test_should_throw_exception_when_bit_count_is_invalid(self):
        with self.assertRaises(InvalidBitCount):
            validate_binary('001001100')
            validate_binary('010')
            validate_binary('')

    def test_should_throw_exception_when_operator_is_invalid(self):
        with self.assertRaises(InvalidOperator):
            validate_operator('^')


class ConversionsTestCase(unittest.TestCase):
    def test_should_convert_to_integer_when_binary_number_is_passed(self):
        self.assertEqual(bin_to_int('11111111'), 255)

    def test_should_throw_exception_when_not_binary(self):
        with self.assertRaises(NotBinary):
            bin_to_int('0b111111')

    def test_should_convert_to_binary_when_integer_number_is_passed(self):
        self.assertEqual(int_to_bin(255), '11111111')

    def test_should_throw_exception_when_not_integer(self):
        with self.assertRaises(NotInteger):
            int_to_bin('00010')


if __name__ == '__main__':
    unittest.main()
