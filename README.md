# Digistarts - Binary Numbers

Make a program capable of operating two numbers from 0-255 in binary.
The answer must also be given in binary.

The following operators must be accepted: ```+, -, *, / and %```.

## Usage

To run this app you'll need:

* Python 3

There is one easy ways to run and test this app.

### Locally

1. Clone this repository.
2. Run: `python main.py`

## Testing

To test, just run:
```
$ python -m unittest
```

## Implementation Details

About the development environment, I'm developing this project using a Notebook,
running openSUSE Leap. I have written this text and code entirely in PyCharm.

To develop this app, I didn't add any extra dependencies.