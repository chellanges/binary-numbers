from ..helpers.conversions import bin_to_int, int_to_bin
from ..helpers.validations import is_binary


class Calculator:
    def __init__(self, number_one, number_two):
        is_binary(number_one)
        is_binary(number_two)

        self.number_one = bin_to_int(number_one)
        self.number_two = bin_to_int(number_two)

    def sum(self):
        result = self.number_one + self.number_two

        return int_to_bin(result)

    def subtract(self):
        result = self.number_one - self.number_two

        return int_to_bin(result)

    def multiply(self):
        result = self.number_one * self.number_two

        return int_to_bin(result)

    def split(self):
        result = int(self.number_one / self.number_two)

        return int_to_bin(result)

    def modulo(self):
        result = self.number_one % self.number_two

        return int_to_bin(result)
