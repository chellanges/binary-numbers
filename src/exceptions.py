class NotString(ValueError):
    pass


class InvalidBitCount(ValueError):
    pass


class NotBinary(ValueError):
    pass


class NotInteger(ValueError):
    pass


class InvalidOperator(ValueError):
    pass
