from ..exceptions import InvalidBitCount, InvalidOperator, NotString


def validate_binary(string):
    if type(string) != str:
        raise NotString
    if len(string) != 8:
        raise InvalidBitCount


def validate_operator(operator):
    operators = ['+', '-', '*', '/', '%']
    if operator not in operators:
        raise InvalidOperator


def is_integer(integer):
    if type(integer) == int:
        return True
    return False


def is_binary(string):
    validate_binary(string)

    set_num = set(string)
    binary = {'0', '1'}

    if set_num == binary or set_num == {'0'} or set_num == {'1'}:
        return True

    return False
