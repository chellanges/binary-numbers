from ..exceptions import NotBinary, NotInteger

from .validations import is_binary, is_integer


def bin_to_int(binary):
    if not is_binary(binary):
        raise NotBinary

    return int(binary, 2)


def int_to_bin(integer):
    if not is_integer(integer):
        raise NotInteger

    return '{0:08b}'.format(integer)
